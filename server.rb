# encoding: utf-8
require 'webrick/https'
require 'rubygems'
require 'open-uri'
require 'optparse'
require 'openssl'
require 'webrick'
require 'sdbm'
require 'yaml'
require 'json'
require 'uri'

class Transporter < WEBrick::HTTPServlet::AbstractServlet
  # Process the request, return response
  def do_GET(request, response)
    status, content_type, body = get_api_source(request)

    response.status = status
    response['Content-Type'] = content_type
    response.body = body

    if status == 200
      response['Access-Control-Allow-Origin'] = "*"
      response['Access-Control-Expose-Headers'] = "x-proxy-session"
      response['X-Proxy-Session'] = $store['_transaction_id']
    end
  end

  def do_OPTIONS(request, response)
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Request-Method"] = "GET,POST"
    response["Access-Control-Expose-Headers"] = \
                        "location,x-request-url,x-final-url,x-proxy-session"
    response["Access-Control-Allow-Headers"] = \
                         "content-type,tunnel-auth,x-api-request-method,\
                          x-proxy-session,x-proxy-login,x-proxy-password"
  end

  def get_api_source(req)
    @req = req
    url = parse_url(@req.unparsed_uri)
    
    return 401, '*/*', 'Permission denied!'  unless ::Auth.accepted?(@req)
    return 404, '*/*', 'Undefined resource!' if url.empty?

    headers = {}

    @req.header.each { |k, v| headers[k] = v.first }
    
    #set our headers
    headers['user-agent'] = "curl/7.32.0"
    headers['accept'] = '*/*'
    headers['accept-encoding'] = 'none'
    headers['authorization'] = headers['tunnel-auth'] || nil
    headers.delete('host')

    data = open(url.first, headers).read
    return 200, '*/*', get_body(data)
  end

  def most_used_request?
    special_header == 'most-used'
  end

  def get_body(data)
    case data
      when "most-used" 
        ::ApiHelperResponce.most_repeated_floods(data)
      when "repeat"
        ::ApiHelperResponce.repeat_flood(data)
      else
        data
    end
  end

  def special_header
    (@req.header['x-api-request-method'].first) rescue ""
  end

  def parse_url(line)
    URI.extract(line)
  end
end

#############################
#######Some of Logick########
#############################

module ApiHelperResponce
  class << self
    def most_repeated_floods(resp)
      fill_db_with_new_ids(resp)
      prepare_responce(data_array_keys).to_json
    end
    
    def repeat_flood(resp)
      ""
    end

    private

    def prepare_json_responce(ids)
      $store.select{|k,v| ids.include?(k)}
                .map{|f| YAML.load(f.last)}
    end

    def fill_db_with_new_ids(resp)
      JSON.parse(resp)["response"].each do |f|
        fill_uuid_data(f)
      end
    end
    
    def fill_uuid_data(item)
      id = item['uuid']
      unless $store.has_key?(rating_key(item['uuid']))
        $store[rating_key(id)] = '0'
      end
      $store[data_key(id)] = YAML.dump(item)
    end

    def rating_key(id)
      "rating-uuid-#{id}"
    end

    def data_key(id)
      "data-uuid-#{id}"
    end

    def get_id(key)
      key.sub(/.*uuid-(.*)/, '\1')
    end

    def mr_array_ids
      $store.select{|f| f.first.scan(/rating-uuid-/).any?}
                .sort_by{|f| -f[1].to_i}
                    .first(10)
                        .map{|f| get_id(f.first)}
    end

    def data_array_keys  
      mr_array_ids.map{|f| data_key(f)}
    end
  end
end

module Auth
  class << self
    def accepted?(req)
      @req = req
      @browser_trz_key = auth_header('x-proxy-session')
      return false if _valid_session_absent? && credentials_invalid?
      set_new_trz_hash
      return true
    end
 
    def _init_credls(_new=false)
      set_new_crednls if crednls_blank? || _new
    end

    private

    def set_new_trz_hash
      $store['_transaction_id'] = random_string
    end

    def random_string
      (0...50).map { ('a'..'z').to_a[rand(26)] }.join
    end

    def auth_header(key)
      (@req.header[key].first || '') rescue ''
    end

    def _valid_session_absent?
      $store['_transaction_id'] != @browser_trz_key
    end

    def credentials_invalid?
      $store['server-login'] != 
            convert_to_hash(auth_header('x-proxy-login')) ||
      $store['server-password'] != 
            convert_to_hash(auth_header('x-proxy-password'))
    end

    def set_new_crednls
      puts "Please input server login:"
        $store['server-login'] = input_by_hash
      puts "Please input server password:"
        $store['server-password'] = input_by_hash
    end

    def convert_to_hash(str) 
      Digest::SHA512.new.update(str).hexdigest
    end

    def input_by_hash
      convert_to_hash(gets.chomp)
    end

    def crednls_blank?
       !($store.key?('server-login') && $store.key?('server-password'))
    end
  end
end

class Server
  def initialize argvs
    $store = SDBM.open('flood_io_db')
    opts = opt_parse(argvs)
    @ssl = opts[:ssl_mode]||false
    ::Auth._init_credls(opts[:new_credls]||false)
  end

  def start!
    # Initialize our WEBrick server
    if $0 == __FILE__ then
      server = WEBrick::HTTPServer.new(config)
      server.mount "/get_from/", Transporter
      trap "INT" do server.shutdown end
      server.start
    end
  end

  private

  def config
    cfg = {:Port => 7777}
    cfg.merge!({
             :SSLEnable => true, 
             :SSLCertificate => cert, 
             :SSLPrivateKey => pkey
        }) if @ssl
    return cfg
  end

  def cert
    OpenSSL::X509::Certificate.new File.read 'cert.pem'
  end

  def pkey
    OpenSSL::PKey::RSA.new File.read 'pkey.pem'
  end

  def opt_parse(argvs)
    options = {}
    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: server.rb [-n]"
      opts.separator ""
      opts.on("-n", "--new-credlns",
              "Require if you wanna setup new credentials for server") do
        options[:new_credls] = true
      end
      opts.on("-s", "--ssl-mode",
              "Require if you wanna start server with ssl tunnel") do
        options[:ssl_mode] = true
      end
    end
    opt_parser.parse!(argvs)
    options
  end
end

Server.new(ARGV).start!










